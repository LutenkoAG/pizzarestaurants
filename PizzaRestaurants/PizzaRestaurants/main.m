//
//  main.m
//  PizzaRestaurants
//
//  Created by User on 22/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
