//
//  PRDetailsViewController.m
//  PizzaRestaurants
//
//  Created by User on 24/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import "PRDetailsViewController.h"
#import "ApiService.h"
#import "PRRestaurant.h"



@interface PRDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *openTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end


@implementation PRDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PRRestaurant *detailItem = [ApiService getCachedRestaurant];
    
    self.nameLabel.text = detailItem.name;
    self.openTimeLabel.text = detailItem.status;
    self.distanceLabel.text = [NSString stringWithFormat:@"%@", detailItem.distance];
    self.addressLabel.text = detailItem.address;
    self.priceLabel.text = detailItem.priceMessage;
    self.phoneLabel.text = detailItem.formattedPhone;
}

@end
