//
//  PRListViewController.m
//  PizzaRestaurants
//
//  Created by User on 23/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import "PRListViewController.h"
#import "PRListTableViewCell.h"
#import "PRDetailsViewController.h"
#import "AppDelegate.h"
#import "ApiService.h"
#import <CoreLocation/CoreLocation.h>
#import "NSDictionary+Extended.h"
#import "UIViewController+Alert.h"

@interface PRListViewController () <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray *restaurantList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSInteger offset;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic,retain) CLLocation *lastLocation;
@property (strong, nonatomic) NSString *locationString;
@property (nonatomic) BOOL coordinatesExist;
@property (nonatomic) NSInteger kLoadingCellTag;

@end

@implementation PRListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.kLoadingCellTag = 1;
    self.offset = 0;
    self.restaurantList = [[NSMutableArray alloc] init];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
}

- (void)getrestaurants {
    __weak __typeof(self)weakSelf = self;
    [ApiService getRestaurants:@"venues/explore" search:@"pizzarestaurants" offset:@(0) location:self.locationString completion:^(BOOL isSuccess, NSArray *restaurant, NSError *error) {
        [weakSelf.restaurantList addObjectsFromArray:restaurant];
         weakSelf.offset = weakSelf.offset + restaurant.count;
        [weakSelf.tableView reloadData];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id selectedObject = [self.restaurantList objectAtIndex:indexPath.row];
    [ApiService cacheRestaurant:selectedObject];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.lastLocation != nil){
        return [self.restaurantList count] + 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < [self.restaurantList count]) {
    static NSString *CellIdentifier = @"RestaurantCell";
    PRListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[PRListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.nameLabel.text = [self.restaurantList[indexPath.row] name];
    cell.distanceLabel.text = [NSString stringWithFormat:@"%@", [self.restaurantList[indexPath.row] distance]];
    cell.openTimeLabel.text = [self.restaurantList[indexPath.row] status];
    return cell;
    } else {
        return [self loadingCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cell.tag == self.kLoadingCellTag) {
        __weak __typeof(self)weakSelf = self;
        [ApiService getRestaurants:@"venues/explore" search:@"pizzarestaurants" offset:@(self.offset) location:self.locationString completion:^(BOOL isSuccess, NSArray *restaurant, NSError *error) {
            
            [weakSelf.restaurantList addObjectsFromArray:restaurant];
             weakSelf.offset = weakSelf.offset + restaurant.count;
            [weakSelf.tableView reloadData];
        }];
    }
}

- (UITableViewCell *)loadingCell {
    UITableViewCell *cell = [[UITableViewCell alloc]
                              initWithStyle:UITableViewCellStyleDefault
                              reuseIdentifier:nil];
    
    UIActivityIndicatorView *activityIndicator =
    [[UIActivityIndicatorView alloc]
     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag = self.kLoadingCellTag;
    
    return cell;
}

#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (self.coordinatesExist) {
        [manager stopUpdatingLocation];
        return;
    }
    
    self.lastLocation = [locations objectAtIndex:0];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:self.lastLocation.coordinate.latitude longitude:self.lastLocation.coordinate.longitude];
    self.locationString = [NSString stringWithFormat:@"%0.1f,%0.1f", loc.coordinate.latitude, loc.coordinate.longitude];
    
    NSLog(@"latitude = %f, longitude = %f", self.lastLocation.coordinate.latitude, self.lastLocation.coordinate.longitude);
    
    if (self.lastLocation.coordinate.latitude != 0 && self.lastLocation.coordinate.longitude != 0) {
        self.coordinatesExist = YES;
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:self.lastLocation.coordinate.latitude longitude:self.lastLocation.coordinate.longitude];
        self.locationString = [NSString stringWithFormat:@"%0.1f,%0.1f", loc.coordinate.latitude, loc.coordinate.longitude];
        [self getrestaurants];
        [manager stopUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if([CLLocationManager locationServicesEnabled]){
        NSLog(@"Location Services Enabled");
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [self showAlertOkWithTitie:@"App Permission Denied" message:@"To re-enable, please go to Settings and turn on Location Service for this app."];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
    }
    
}

@end
