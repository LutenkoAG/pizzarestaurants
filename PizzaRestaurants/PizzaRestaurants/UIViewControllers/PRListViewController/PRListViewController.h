//
//  PRListViewController.h
//  PizzaRestaurants
//
//  Created by User on 23/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRDetailsViewController;

#import <CoreData/CoreData.h>

@interface PRListViewController : UIViewController

@property (strong, nonatomic) PRDetailsViewController *detailViewController;

@end
