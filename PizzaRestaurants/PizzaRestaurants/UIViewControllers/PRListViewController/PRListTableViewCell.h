//
//  PRListTableViewCell.h
//  PizzaRestaurants
//
//  Created by User on 23/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *openTimeLabel;

@end
