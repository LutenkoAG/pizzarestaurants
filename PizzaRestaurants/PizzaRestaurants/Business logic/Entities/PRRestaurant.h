//
//  PRRestaurant.h
//  PizzaRestaurants
//
//  Created by User on 28/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRRestaurant : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *distance;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *formattedPhone;
@property (nonatomic, retain) NSString *priceMessage;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
