//
//  PRRestaurant.m
//  PizzaRestaurants
//
//  Created by User on 28/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import "PRRestaurant.h"
#import "NSDictionary+Extended.h"

@implementation PRRestaurant

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.name = [dictionary tryObjectForKey:@"name"];
        self.distance = [[dictionary tryObjectForKey:@"location"] tryObjectForKey:@"distance"];
        self.status = [[dictionary tryObjectForKey:@"hours"] tryObjectForKey:@"status"];
        self.priceMessage = [[dictionary tryObjectForKey:@"price"] tryObjectForKey:@"message"];
        self.formattedPhone = [[dictionary tryObjectForKey:@"contact"] tryObjectForKey:@"formattedPhone"];
        self.address = [[dictionary tryObjectForKey:@"location"] tryObjectForKey:@"address"];
    }
    return self;
}

@end
