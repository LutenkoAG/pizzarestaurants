//
//  ApiService.h
//  PizzaRestaurants
//
//  Created by User on 23/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "PRRestaurant.h"


@interface ApiService : NSObject

+ (ApiService *)sharedApiService;

+ (void)cacheRestaurant:(PRRestaurant *)restaurant;

+ (PRRestaurant *)getCachedRestaurant;

+ (void)getRestaurants:(NSString *)URLString search:(NSString *)search offset:(NSNumber *)offset location:(NSString *)location completion:(void(^)(BOOL isSuccess, NSArray *restaurant, NSError *error))completion;

@end
