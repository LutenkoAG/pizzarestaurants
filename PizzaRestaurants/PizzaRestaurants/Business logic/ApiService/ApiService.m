//
//  ApiService.m
//  PizzaRestaurants
//
//  Created by User on 23/10/16.
//  Copyright © 2016 LutenkoAG. All rights reserved.
//

#import "ApiService.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "NSDictionary+Extended.h"

static NSString * const kBasePath = @"https://api.foursquare.com/v2";
static NSString * const kVenusSearch = @"venues/search";
static NSString * const kVenuesExplore = @"venues/explore";
static NSString * const kClientID = @"XKMRPW4DW1PQKGM3ODB4OUWA1NHJLSFIZFXNDRF3SN4AQBKG";
static NSString * const kClientSecret = @"A12EK3S3NXLMVXWFSNQBTNR1OJOQ5Z43WSCCI2AVIWG12VSO";
static NSString * const kVersionParameter = @"20161017";

NSUInteger const limitRestaurant = 10;

@interface ApiService ()

@property (strong, nonatomic) id currentRestaurant;
@property (strong, nonatomic) NSArray *currentRestaurantsList;

@end

@implementation ApiService

+ (ApiService *)sharedApiService {
    static ApiService *_sharedApiService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedApiService = [ApiService new];
    });
    return _sharedApiService;
}

+ (void)getRestaurants:(NSString *)URLString search:(NSString *)search offset:(NSNumber *)offset location:(NSString *)location completion:(void(^)(BOOL isSuccess, NSArray *restaurant, NSError *error))completion {
    
    NSDictionary *parameters = @{@"ll" : location,
                                 @"query" : search,
                                 @"limit" : @(limitRestaurant),
                                 @"sortByDistance" : @(1),
                                 @"offset" : offset};
    
    NSMutableDictionary *allParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [allParameters setObject:kClientID forKey:@"client_id"];
    [allParameters setObject:kClientSecret forKey:@"client_secret"];
    [allParameters setObject:kVersionParameter forKey:@"v"];
    
    NSLog(@"URL: %@\nParameters: %@", URLString, allParameters);
    NSString *urlString = [NSString stringWithFormat:@"%@", URLString];
    NSLog(@"\nPOST\n URL: %@/%@", kBasePath, urlString);

    [self getRequest:urlString
          parameters:allParameters
             headers:@{@"Content-Type": @"application/json"}
          completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
              if (error) {
                  completion(NO, nil, nil);
              } else {
                  id json = [NSJSONSerialization JSONObjectWithData:data
                                                            options:0
                                                              error:nil];
                  
                  NSDictionary *items = [[[[json objectForKey:@"response"] objectForKey:@"groups"] objectAtIndex:0] objectForKey:@"items"];
                  
                  NSMutableArray *array = [[NSMutableArray alloc] init];
                  for (NSDictionary *dict in [items valueForKey:@"venue"]) {
                      PRRestaurant *restauran = [[PRRestaurant alloc] initWithDictionary:dict];
                      [array addObject:restauran];
                      NSLog(@"restauran: %@", restauran.name);
                  }
                  
                  NSArray *restaurants = [NSArray arrayWithArray:array];

                  completion(YES, restaurants, nil);
              }
          }];
}

+ (void)cacheRestaurant:(PRRestaurant *)restaurant {
    ((ApiService *)[self sharedApiService]).currentRestaurant = restaurant;
}

+ (PRRestaurant *)getCachedRestaurant {
    return ((ApiService *)[self sharedApiService]).currentRestaurant;
}

#pragma mark -
#pragma mark Private

+ (NSURL *)url:(NSString *)subPath {
    return [self url:subPath parameters:nil];
}

+ (NSURL *)url:(NSString *)subPath parameters:(NSDictionary *)parameters {
    if ([[parameters allKeys] count]) {
        NSMutableString *parametersString = [[NSMutableString alloc] init];
        
        [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [parametersString appendFormat:@"%@=%@&", key, obj];
        }];
        
        return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?%@", kBasePath, subPath, parametersString]];
    }
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", kBasePath, subPath]];
}

+ (void)getRequest:(NSString *)urlString
        parameters:(NSDictionary *)parameters
           headers:(NSDictionary *)headers
        completion:(void(^)(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error))completion {
    
    NSLog(@"\nGET\n URL: %@/%@\nParameters: %@", kBasePath, urlString, parameters);
    
    NSURL *url = [self url:urlString parameters:parameters];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    
    if ([[headers allKeys] count]) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [request addValue:obj forHTTPHeaderField:key];
        }];
    }
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error) {
                
                                                                     completion((error == nil), data, response, error);
                                                                 }];
    [task resume];
}
    
@end
